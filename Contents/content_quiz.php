  <style type="text/css">
      .container {
  width: 300px;
  clear: both;
}

.container input {
  width: 100%;
  clear: both;
}

body{ font: 14px Georgia, serif; }
 
#page-wrap { width: 500px; margin: 0 auto;}
 
h1  { margin: 25px 0; font: 18px Georgia, Serif; text-transform: uppercase; letter-spacing: 3px; }
 
#quiz input {
    vertical-align: middle;
}
 
#quiz ol {
   margin: 0 0 10px 20px;
}
 
#quiz ol li {
   margin: 0 0 20px 0;
}
 
#quiz ol li div {
   padding: 4px 0;
}
 
#quiz h3 {
   font-size: 17px; margin: 0 0 1px 0; color: #666;
}
 
#results {
    font: 44px Georgia, Serif;
}
  </style>

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <?php include 'Contents/header_quiz.php'; ?>
      <div id="page-wrap">
    
  <form action="result.php" method="post" id="quiz">
    
            <ol>
            
                <li>
                
                    <h3>Gastroenterologie se zabývá onemocněním...</h3>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-A" value="A" CHECKED/>
                        <label for="question-1-answers-A">A) Plic</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-B" value="B" />
                        <label for="question-1-answers-B">B) Kůže</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-C" value="C" />
                        <label for="question-1-answers-C">C) Trávicích orgánů</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-1-answers" id="question-1-answers-D" value="D" />
                        <label for="question-1-answers-D">D) Jater</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Hematokrit je především závislý na koncentraci...</h3>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-A" value="A" CHECKED/>
                        <label for="question-2-answers-A">A) Hemoglobinu</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-B" value="B" />
                        <label for="question-2-answers-B">B) Trombocytů</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-C" value="C" />
                        <label for="question-2-answers-C">C) Leukocytů</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-2-answers" id="question-2-answers-D" value="D" />
                        <label for="question-2-answers-D">D) Erytrocytů</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Katarakta je....</h3>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-A" value="A" CHECKED/>
                        <label for="question-3-answers-A">A) Zákal oční čočky</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-B" value="B" />
                        <label for="question-3-answers-B">B) Katetrizace</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-C" value="C" />
                        <label for="question-3-answers-C">C) Zánět sliznice dýchacích orgánů</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-3-answers" id="question-3-answers-D" value="D" />
                        <label for="question-3-answers-D">D) Katar</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>K invazivním metodám nepatří..</h3>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-A" value="A" CHECKED/>
                        <label for="question-4-answers-A">A) Punkce</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-B" value="B" />
                        <label for="question-4-answers-B">B) Rentgenové vyšetření</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-C" value="C" />
                        <label for="question-4-answers-C">C) Operace</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-4-answers" id="question-4-answers-D" value="D" />
                        <label for="question-4-answers-D">D) Gastroskopie</label>
                    </div>
                
                </li>
                
                <li>
                
                    <h3>Pacient s hypotonií má...</h3>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-A" value="A" CHECKED/>
                        <label for="question-5-answers-A">A) Příliš vysokou hladinu cukru v krvi</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-B" value="B" />
                        <label for="question-5-answers-B">B) Příliš nízkou hladinu cukru v krvi</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-C" value="C" />
                        <label for="question-5-answers-C">C) Příliš vysoký krevní tlak</label>
                    </div>
                    
                    <div>
                        <input type="radio" name="question-5-answers" id="question-5-answers-D" value="D" />
                        <label for="question-5-answers-D">D) Příliš nízký krevní tlak</label>
                    </div>
                
                </li>
            
            </ol>
            
            <input type="submit" value="Vyhodnotit" class="submitbtn" />
    
    </form>
  
  </div>
    </div>
    <?php include 'Contents/ads.php'; ?>

