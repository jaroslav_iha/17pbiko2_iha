<style type="text/css">
      .container {
  width: 300px;
  clear: both;
}

.container input {
  width: 100%;
  clear: both;
}
table {
  width:100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 15px;
  text-align: left;
}
table#t01 tr:nth-child(even) {
  background-color: #eee;
}
table#t01 tr:nth-child(odd) {
 background-color: #fff;
}
table#t01 th {
  background-color: black;
  color: white;
}
</style>


  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-teal">Studijní materiály</h1>
        <table>
          <tr>
            <th>Téma</th>
            <th>Přednášející</th> 
            <th>Zdroj</th>
          </tr>
          <tr>
            <td>Vybrané onemocnění gastrointestinálního traktu</td>
            <td>as. MUDr. Tomáš Heřman</td>
            <td><a href="Materials/GIT.ppt" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Hematologie</td>
            <td>prof. MUDr. Leoš Navrátil, CSc., MBA, dr.h.c.</td>
            <td><a href="Materials/Hematologie.pdf" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Pokroky v operačních oborech</td>
            <td>as. MUDr. Petr Jelínek</td>
            <td>prezentace</td>
          </tr>
          <tr>
            <td>Základy laboratorní medicíny</td>
            <td>prof. MUDr. Jaroslav Racek, DrSc.</td>
            <td><a href="Materials/Labo.pdf" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Vybrané kapitoly z anesteziologie</td>
            <td>doc. MUDr. Ladislav Hess, DrSc.</td>
            <td><a href="Materials/Anest.pdf" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Nefrologie</td>
            <td>prof. MUDr. Jaroslav Racek, DrSc.</td>
            <td><a href="Materials/Nefrologie.ppt" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Základy endokrinologie. Diagnostické metody, využití přístrojové techniky
Diabetes mellitus</td>
            <td>as. MUDr. Tomáš Heřman</td>
            <td><a href="Materials/Endokrinologie.ppt" download>prezentace 1</a>, <a href="Materials/Dia1.ppt" download>prezentace 2</a>, <a href="Materials/Dia2.pdf" download>prezentace 3</a></td>
          </tr>
          <tr>
            <td>Základní diagnostické a terapeutické postupy v dermatologii a venerologii</td>
            <td>MUDr. Alena Ševčíková</td>
            <td><a href="Materials/Dermatovenerologie.pdf" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Základní diagnostické a terapeutické postupy ve stomatologii</td>
            <td>MDDr. Jan Šenfeld</td>
            <td>prezentace</td>
          </tr>
          <tr>
            <td>Základní diagnostické a terapeutické postupy v oftalmologii</td>
            <td>doc. MUDr. Ján Lešták, CSc. FEBO, MBA</td>
            <td><a href="Materials/Oftamologie.pdf" download>prezentace</a></td>
          </tr>
          <tr>
            <td>Akutní nemoc z ozáření, diagnostika, léčba. Akutní stavy na základě</td>
            <td>prof. MUDr. Leoš Navrátil, CSc., MBA, dr.h.c.</td>
            <td>prezentace</td>
          </tr>
          <tr>
            <td>Patopsychologie</td>
            <td>as, Mgr. Monika Donevová</td>
            <td>prezentace</td>
          </tr>
          <tr>
            <td>Základy fyzioterapie</td>
            <td>prof. MUDr. Leoš Navrátil, CSc., MBA, dr.h.c.</td>
            <td>prezentace</td>
          </tr>
        </table>                        
    </div>
    <?php include 'Contents/ads.php'; ?>

