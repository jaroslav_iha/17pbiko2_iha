<div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
    <a href="index.php" class="w3-bar-item w3-button w3-theme-l1">Domů</a>
    <a href="about.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">O nás</a>
    <a href="contact.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Kontakt</a>
    <a href="#" style="float: right;" class="w3-bar-item w3-button w3-hide-small w3-hover-white" ><?php
    $dt = new DateTime();
    echo $dt->format('d. m. Y');?></a>
  </div>